const axios = require('axios');
const phdAPI = axios.create({
  baseURL: 'https://services-staging.digital.pizzahut.com/phdapi/v1',
});

phdAPI.defaults.params = {};
phdAPI.defaults.params['key'] = 'RyAzYIiBQMB6ISN8CAOiulwmGHA68F4c';
phdAPI.defaults.headers['akaatornr'] = 'X6mgXChU4Ssu6rmF';
phdAPI.defaults.headers['ConversationID'] = '123456789';
const phPay = axios.create({
  baseURL: 'https://services-staging.digital.pizzahut.com/phpay/v1',
});

phPay.defaults.params = {};
phPay.defaults.params['key'] = 'RyAzYIiBQMB6ISN8CAOiulwmGHA68F4c';
phPay.defaults.headers['akaatornr'] = 'X6mgXChU4Ssu6rmF';
phPay.defaults.headers['ConversationID'] = '123456789';
const loginData = {
  username: 'just.peachy@staypeachy.com',
  password: 'JustPeachy',
  channel_id: 'PizzaHut'
};
const storeSearchData = {
  "address1": "3009 N. Clark St.",
  "address2": "",
  "address3": "",
  "address4": "",
  "city": "Chicago",
  "state": "IL",
  "postal_code": "60657",
  "geocode_validate": true,
  "limit": 0,
  "occasion_id": "DELIVERY"
};

const login = async () => {
  console.log('login');
  try {
    const { data } = await phdAPI.post('/customer/login', loginData);
    return data.auth_token;
  } catch (error) {
    console.log({ error: error.toString(), message: error.message });
  };
};
const localize = async () => {
  console.log('localize');
  try {
    const { data } = await phdAPI.post('/stores/search', storeSearchData);
    return data[0].store_number;
  } catch (error) {
    console.log({ error: error.toString(), message: error.message });
  };
};
const createCart = async (storeNumber) => {
  console.log(`CreateCart using storeNumber: ${storeNumber}`);
  try {
    const cartCreationData = {
      store_number: storeNumber,
      occasion_id: 'DELIVERY',
      delivery_address: {
        address1: storeSearchData.address1,
        address2: storeSearchData.address2,
        address3: storeSearchData.address3,
        address4: storeSearchData.address4,
        city: storeSearchData.city,
        state: storeSearchData.state,
        postal_code: storeSearchData.postal_code,
      },      
    };
    const { data } = await phdAPI.post('/carts', cartCreationData);
    return data.cart_id;
  } catch (error) {
    console.log({ error: error.toString(), message: error.message });
  };
};
const populateCart = async (cartID, storeNumber) => {
  console.log(`PopulateCart using storeNumber: ${storeNumber} and cartID: ${cartID}`);
  try {
    const cartProducts = {
      products: Array(3).fill(
        {
          "product_id": `11131~P^B~${storeNumber}`,
          "quantity": 5,
          "size_id": "P~L",
          "modifiers": [
              {
                  "modifier_id": `C~H~${storeNumber}`,
                  "quantities": [
                      13
                  ]
              },
              {
                  "modifier_id": `S~PS~${storeNumber}`,
                  "quantities": [
                      1
                  ]
              },
              {
                  "modifier_id": `V~BO~${storeNumber}`,
                  "quantities": [
                      2,
                      1
                  ]
              }
          ]
        }
      ),
    };
    await phdAPI.post(`/carts/${cartID}/products/multi`, cartProducts);
  } catch (error) {
    console.log({ error: error.toString(), message: error.message });
  };
};
const tokenizeCC = async () => {
  console.log('tokenizeCC');
  try {
    const cartInformation = {
      "expiration_month": 1,
      "expiration_year": 27,
      "pan": '6011111111111117'
    };
    const { data: { token: cardToken } } = await phPay.post(`/tokenize`, cartInformation);
    return cardToken;
  } catch (error) {
    console.log({ error: error.toString(), message: error.message });
  };
};
const sendOrder = async (cartID, cardToken) => {
  console.log(`SendOrder using cartToken: ${cardToken} and cartID: ${cartID}`);
  try {
    await phPay.post('/orders/tokenized', {
      cart_id: cartID,
      customer: {
          email: loginData.username,
          first_name: "creditcard",
          last_name: "you",
          coppa_agreement: true,
          verified_age_gate: true,
          delivery_address: {
              address1: "100 Memory ln",
              address2: "1419",
              address3: "",
              address4: "",
              city: "plano",
              state: "TX",
              postal_code: "75024"
          },
          phone: "214-111-2222",
          phone_extension: ""
      },
      delivery_instructions: "Ring",
      gratuity: 0,
      forms_of_payment: [
          {
              type: "creditcard",
              name: "Main card",
              metadata: {
                  type: 'DISC',
                  card_code: '456',
                  card_mask: '601111******1117',
                  postal_code: "60654",
                  expiration: "01/27",
                  gateway: "VERIFONE",
                  token: cardToken
              }
          }
      ]
    });
  } catch (error) {
    console.log({ error: error.toString(), message: error.message });
  };
};
const clearCart = async (cartID) => {
  console.log(`ClearCart using cartID: ${cartID}`);
  try {
    await phdAPI.post(`/carts/${cartID}/clear`);
  } catch (error) {
    if (error?.response?.data?.statusCode !== '200')
      console.log({ error: error.toString(), message: error.message });    
  };
}

const start = async () => {
  console.log('start');
  let cartID = null;
  try {
    // Login
    const authToken = await login();
    phdAPI.defaults.headers['Auth-Token'] = authToken;
    phPay.defaults.headers['Auth-Token'] = authToken;
    // Localize
    const storeNumber = await localize();
    // Create Cart
    cartID = await createCart(storeNumber);
    // Populate Cart
    await populateCart(cartID, storeNumber);
    // Tokenize CC
    const cardToken = await tokenizeCC();
    // Post Payment
    await sendOrder(cartID, cardToken);
  } catch (error) {
    console.log('Main Catch', { error: error.toString(), message: error.message });
  } finally {
    // Clear cart
    if (cartID)
      await clearCart(cartID);
  };
};

start();